"""
Module to process XML bilingual WordFast files. It reads the 2 formats of WF files, the old (TXML) and the new one (TXLF).
It allows to extract all the necessary information to process these files, like getting the target language or source language.
The classes of this module allow to read and modify existing WordFast files. It does not allow to generate from scratch.

This version of xml_wf is incompatible with the previous ones like the library version 0.9.

It is structured in composition classes and the main class and callable class is Wf and WfGroup.
Wf class generates an object with all the contents of the WF file and the tools to handle these kind of files.
Attributes:
    txlf_nsmap: Dictionary that contains the namespaces for TXLF files.

TODO:
    Sphinx documentation
"""
__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2018. All rights are reserved.'
__license__ =  'GPL 3'


from pathlib import Path
from lxml import etree as et
import re
from os import getlogin
from datetime import datetime
import warnings
from copy import deepcopy


txlf_nsmap = {'gs4tr': 'http://www.gs4tr.org/schema/xliff-ext', 
                        'Default': 'urn:oasis:names:tc:xliff:document:1.2'
                        }

class WfGroup2:
    """
    Improved Group class allowing to access to the objects through index or object name
    """
    __wf_group = []
    def __init__(self, parent: object = None):
        self.__trash_bin = []
        self.__indexes = dict()
        self._index = 0
        self._last_item = len(self.__wf_group) - 1
        self._parent = parent
    
    def __getitem__(self, key: object):
        """
        """
        if isinstance(key, int):
            if key in self.__indexes:
                return self.__wf_group[key]
        else:
            for k, value in self.__indexes.items():
                if isinstance(key, str):
                    if key == value.wf_file:
                        return self.__wf_group[k]
                elif value == key:
                    return self.__wf_group[k]
            raise KeyError(f'The wf file with key <{key}> does not exist.')
    
    def __iter__(self):
        return self.__wf_group
    
    def __next__(self):
        if self._index < len(self.__wfgroup):
            self._index += 1
            return self.__wf_group[self._index]
        else:
            raise StopIteration('Exhausted the number of WF files in this collection. '
                                        'It was the last.')
    
    def __len__(self):
        return self.__wf_group
    
    def __repr__(self):
        return self.__wf_group
    
    def __str__(self):
        return f'<WfGroup2 {self._group}>'
    
    def __bool__(self):
        if len(self.__wf_group) > 0:
            return True
        else:
            return False
    
    def add(self, wf: object):
        idx = len(self.__wf_group)
        self.__wf_group.append(wf)
        self.__indexes[idx] = wf


class WfGroup:
    """
    Iterator class that allows to add different Wf files to it.
    Internally stores the Wf object file in a dictionary, using as key the Wf filename and as value the Wf object.
    """
    def __init__(self):
        """
        Store the wf objects. 
        Initializes the variables.
        Private attributes:
            __wf_group: Dictionary where are stored the Wf objects
            __trash_bin: Stores the deleted objects. Allowing persistence.
        """
        self.__wf_group = dict()
        self.__trash_bin = []
        self.index = 0
    
    def __iter__(self):
        return iter(self.__wf_group)
    
    def __contains__(self, wf):
        return self.has(wf)
    
    def __getitem__(self, key):
        return self.__wf_group[key]
    
    def append(self, wf_object):
        """
        Add/append a new Wf object to the list
        """
        
        if isinstance(wf_object, Wf):
            self.__wf_group[str(wf_object.wf_file_path.name)] = wf_object
            wf_object.append_group(self)
    
    def wf_group(self):
        """
        Get the list of Wf objects existing in the dictionary.
        """
        return list(self.__wf_group)
    
    def delete(self, wf_object):
        """
        Remove a wf_object from the list and add it to the trash bin
        Arguments:
        wf_objects: Object file or object_files to be deleted
        """
        
        if isinstance(wf_object, Wf):
            if self.__has_internal(str(wf_object.wf_file_path.name)):
                self.__delete_internal(wf_object)
    
    def __delete_internal(self, wf_object):
        deleted = self.__wf_group[wf_object]
        if deleted:
            self.__trash_bin.append(wf_object)
        del self.__wf_group[wf_object.wf_file_path.name]
    
    def __has_internal(self, wf_object):
        """
        Private method to check if a WF object is already in the list.
        """
        return wf_object in self.__wf_group
    
    def has(self, wf_object):
        """
        Check if the wf_object already exist in WfGroup
        Arguments:
        wf_object: Object Wf to check if it is contained in the group or not
        """
        res_value = False
        if isinstance(wf_object, Wf):
            if self.__has_internal(str(wf_object.wf_file_path.name)):
                res_value = True
        return res_value
                

class SegmentContent:
    """
    Composition class for the segment. It allows to create the source or/and
    the target elements in the XML structure of the WF files (TXML and TXLF).
    Here is where is stored the translatable and translated content.
    
    Arguments:
        value (str): It only accepts 2 values: source or target.
        xml_seg (et.Element): XML Element of the Segment. Part of ElementTree.
        wf_version (str): Version of WordFast. 'txml' or 'txlf'
    """
    def __init__(self, value, xml_seg, wf_version):
        """
        Initialization of the file.
        Arguments:
            value(str): It only accepts 2 values: source or target
        """
        self._xml_seg = xml_seg
        self.value = value
        self._xml = ''
        self.wf_version = wf_version
        self._text = ''
        self._only_text = ''
        self._xml_string = ''
        self.__attributes = dict()
        self.__seg_info = dict()
        try:
            if self.wf_version == 'txlf':
                    self._xml = self._xml_seg.xpath(
                            'Default:{}'.format(value), namespaces=txlf_nsmap)[0]
            else:
                self._xml = self._xml_seg.find('{}'.format(value))
                #self._xml = self._xml_seg.xpath('{}'.format(value))[0]
                #print(self._xml)
        except:
            self._xml_string = ''
            self._xml = ''
            self._only_text = ''
            self._attributes = dict()
            self._text = ''

    @property
    def xml_string(self):
        """
        """
        if self._xml is not None:
            
            try:
                return et.tostring(self._xml)
            except TypeError:
                return self._xml
        else: 
            return ''

    @property
    def attributes(self):
        """
        List the attributes of the SegmentContent, target or source.
        """
        if not self.__attributes:
            if self._xml is not None:
                for attribute, value in self._xml.attrib.items():
                    self.__attributes[attribute] = value
        else:
            return self.__attributes
    
    def get_attribute(self, attribute):
        """
        Get the value of the indicated attribute.
        Arguments:
            attribute (str): Name of the attribute to retrieve.
        Return:
            value(str): The value of the indicated attribute.
            If it is empty or does not exist return None.
        """
        if not self.__attributes and self._xml is not None:
            for attr, value in self._xml.attrib.items():
                if attr == attribute:
                    return value
        elif self.__attributes:
            for attr,  value in self.__attributes:
                if attr == attribute:
                    return value
        elif not self.__attributes and self._xml is None:
            #print(self._xml, self._xml_seg.find('target'))
            return None
    
    def set_seg_info(self, username: str, timestamp: str) -> None:
        """
        Add to the specific SegmentContent, specially for target
        the seginfo information that includes username and time stamp.
        
        Arguments:
            username (str): Username that made the change in the SegmentContent
            timestamp (str): Date that the change was introduced. Formatted as: 20170911T180211Z
        """
        self.__seg_info['username'] = username
        self.__seg_info['timestamp'] = timestamp
        if self.wf_version == 'txlf':
            try:
                seg_info = self._xml.attrib[
                                         '{http://www.gs4tr.org/schema/xliff-ext}seginfo']
                seg_info_attributes_xml = et.fromstring(seg_info)
            except KeyError:
                seg_info_attributes_xml = et.Element('root')
            seg_info_attributes_xml.attrib['username'] = username
            seg_info_attributes_xml.attrib['timestamp'] = timestamp
        if self.wf_version == 'txml':
            self._xml.attrib['creationid'] = username
            self._xml.attrib['creationdate'] = timestamp
    
    def set_attribute(self, attribute, value):
        """
        Add or modidfy an existing attribute.
        Warning: If an attribute needs to be added to a TXLF file,
        it is important to know if the attribute has namespaces, 
        for example gs4tr:score.
        The namespaces for gs4tr would be {http://www.gs4tr.org/schema/xliff-ext}.
        So to add the attribute score in a TXLF file would be done as 
        the following example:
        
        Example:
            set_attribute('{http://www.gs4tr.org/schema/xliff-ext}score', '100')
        
        Arguments:
            attribute (str): Attribute to modify or add.
        """
        self.__attributes[attribute] = value
        if self._xml is not None:
            self._xml.attrib[attribute] = value
    
    def delete_attribute(self, attribute):
        """
        Delete an attribute.
        Warning: If an attribute needs to be added to a TXLF file,
        it is important to know if the attribute has namespaces, 
        for example gs4tr:score.
        The namespaces for gs4tr would be {http://www.gs4tr.org/schema/xliff-ext}.
        So to add the attribute score in a TXLF file would be done as 
        the following example:
        
        Example:
            delete_attribute('{http://www.gs4tr.org/schema/xliff-ext}score')
        
        Arguments:
            attribute (str): Attribute to modify or add.
        """
        if self._xml is not None:
            #print(attribute, self._xml.attrib)
            if attribute in self._xml.attrib:
                del self._xml.attrib[attribute]
    
    def delete_all_attributes(self):
        """
        Delete all attributes of the SegmentContent. It is used to leave
        without any attribute the <target> element.
        """
        if self._xml is not None:
            for key in self._xml.attrib.keys():
                del self._xml.attrib[key]
    
    def convert_ws_to_string(self):
        """
        """
        if self.wf_version == 'txlf':
                ws_tag = r'{http://www.gs4tr.org/schema/xliff-ext}ws'
        else:
            ws_tag = 'ws'
        for elem in self._xml_seg:
            if elem.tag == ws_tag:
                if len(self.xml) > 0:
                    if self.xml[-1].tail:
                        self.xml[-1].tail += elem.text
                    else:
                        self.xml[-1].tail = elem.text
                else:
                    print('Element:', self.xml, elem.text,  elem)
                    self.xml.text = f'{self.xml.text}{elem.text}'
                    print('Convert ws to string', self.xml)
                elem.getparent().remove(elem)
    
    @property
    def __text(self):
        """
        Get the text including the tags
        """
        warnings.warn('Old text implementation. To be deleted in future iterations', DeprecationWarning)
        if self._xml is not None and self._xml != '':
            xml_temp = self.__nodes_browse(False)
            return xml_temp.text
        else:
            return ''

    @property
    def text(self):
        """
        Get the text without tags. Replacing them with its internal text if it is applicable.
        """
        return self.__text_visualize()
    
    @text.setter
    def text(self, value):
        if self._xml is not None and self._xml != '':
            for elem in self._xml.iterchildren():
                elem.getparent().remove(elem)
        else:
            self._xml = et.SubElement(self._xml_seg, self.value)
        self._xml.text = value
    
    @property
    def text_tagged(self):
        """
        Get the text with the tags without replacing them with its internal text.
        """
        return self.__text_visualize(delete_text=True, replace_tag=True)
    
    @property
    def __all_text(self):
        """
        It displays the text including the text between tags.
        This function may be useful to calculate the maxlen of
        a sentence or similar.
        """
        warnings.warn('It is deprecated and hidden', DeprecationWarning)
        if self._xml is not None:
            xml_temp = self.__nodes_browse_text()
            return xml_temp.text
        else:
            return ''

    @property
    def text_tagged_ws(self):
        """
        Return the text with trailing spaces and without replacing tags.
        """
        if self.text_tagged:
            if self.wf_version == 'txlf':
                ws_tag = r'{http://www.gs4tr.org/schema/xliff-ext}ws'
            else:
                ws_tag = 'ws'
            full_all_text = ''
            for elem in self._xml_seg.iterchildren():
                if elem.tag == ws_tag:
                    if elem.text:
                        #print(elem.text)
                        full_all_text += elem.text
                if elem.tag == self._xml.tag:
                    full_all_text += self.text_tagged
            return full_all_text
        else:
            return ''
    
    @property
    def text_ws(self):
        """
        Display all the text including leading and trailing spaces. 
        Those are the spaces included in the <ws> tags in TXLF or TXML files.
        """
        if self.text:
            if self.wf_version == 'txlf':
                ws_tag = r'{http://www.gs4tr.org/schema/xliff-ext}ws'
            else:
                ws_tag = 'ws'
            full_all_text = ''
            for elem in self._xml_seg.iterchildren():
                if elem.tag == ws_tag:
                    if elem.text:
                        #print(elem.text)
                        full_all_text += elem.text
                if elem.tag == self._xml.tag:
                    full_all_text += self.text
            return full_all_text
        else:
            return ''

    @property
    def target_score(self) -> int:
        """
        The target score that has the segment. It indicates its leverage.
        """
        try:
            if self.value == 'target':
                if self.wf_version == 'txlf':
                    return int(self.get_attribute(
                                             '{http://www.gs4tr.org/schema/xliff-ext}score'))
                else:
                    return int(self.get_attribute('score'))
        except KeyError:
            return None
    
    @property
    def rep_score(self):
        """
        The score of internal fuzzies/repetitions.
        Only supported in TXLF files.
        """
        try:
            if self.value == 'target':
                if self.wf_version == 'txlf':
                    return int(self.get_attribute(
                                              '{http://www.gs4tr.org/schema/xliff-ext}repscore'))
                else:
                    raise TypeError('This property is not supported in TXML files.')
        except KeyError:
            return None
    
    @property
    def score(self):
        """
        Alias of target_score
        """
        return self.target_score
    
    @property
    def state_qualifier(self):
        """
        Returns the qualifier of the contents. It is normally with the values:
            [exact-match, x-context-match, fuzzy-match]
        """
        try:
            if self.value == 'target':
                if self.wf_version == 'txlf':
                    return self.get_attribute('state-qualifier')
                else:
                    raise TypeError('Property not supported for TXML files')
        except KeyError:
            return None
    
    @property
    def xml_text(self):
        """
        """
        warnings.warn('This property is deprecated use xml instead.', DeprecationWarning)
        if self._xml is not None:
            return self._xml
        else:
            return None
    
    @xml_text.setter
    def xml_text(self, xml_text: et.Element):
        warnings.warn('This property is deprecated use xml instead.', DeprecationWarning)
        if isinstance(xml_text, et.Element):
            self._xml = xml_text
        else:
            raise Exception.TypeError('The type is not valid. It should be an XML Element')
    
    @property
    def xml(self):
        """
        Shows and allows to update the xml of theSegmentContent.
        """
        if self._xml is not None:
            return self._xml
        else:
            return None
    
    @xml.setter
    def xml(self, xml_text: et.Element):
        if isinstance(xml_text, et.Element):
            self._xml = xml_text
        else:
            raise Exception.TypeError('The type is not valid. It should be an XML Element')
    
    def replace_tag_keeping_tail(self, element):
        """
        Safely replace an element
        """
        self._preserve_tail_before_replacement(element)
        element.getparent().remove(element)
    
    def get_number_tags(self, text: str='SoftReturn'):
        """
        Find the number of tags that have the segment breaker content in the SegmentContent object.
        """
        counter = 0
        for tag in self._xml.getchildren():
            if text in tag.text:
                counter += 1
        return counter
    
    def _preserve_tail_before_replacement(self, node):
        if node.tail: # preserve the tail
            previous = node.getprevious()
            tail = r'{tag}' + str(node.tail)

            if previous is not None: # if there is a previous sibling it will get the tail
              
                if previous.tail is None:
                    
                    previous.tail =  tail
                else:
                    previous.tail = previous.tail + tail
            else: # The parent get the tail as text
                parent = node.getparent()
                if parent.text is None:
                    parent.text = tail
                else:
                    parent.text = parent.text + tail
    
    def remove_keeping_tail(self, element):
        """Safe the tail text and then delete the element"""
        # Solution from Stackoverflow:
        # https://stackoverflow.com/questions/42932828/how-delete-tag-from-node-in-lxml-without-tail
        self._preserve_tail_before_delete(element)
        element.getparent().remove(element)

    def _preserve_tail_before_delete(self, node):
        """
        
        """
        equiv_text = ''
        if node.tail: # preserve the tail
            previous = node.getprevious()
            if previous is not None: # if there is a previous sibling it will get the tail
                if previous.tail is None:
                    if node.tag == '{urn:oasis:names:tc:xliff:document:1.2}x':
                        equiv_text = ''
                        if node.attrib['ctype'] == 'x-tab':
                            equiv_text = '\t'
                        if node.attrib['ctype'] == 'lb':
                            equiv_text = '\t'
                        previous.tail = equiv_text + node.tail
                    else:
                        previous.tail = node.tail
                else:
                    if node.tag == '{urn:oasis:names:tc:xliff:document:1.2}x':
                        equiv_text = ''
                        if node.attrib['ctype'] == 'x-tab':
                            equiv_text = '\t'
                        if node.attrib['ctype'] == 'lb':
                            equiv_text = '\t'
                        previous.tail = previous.tail + equiv_text + node.tail
                    else:
                        previous.tail = previous.tail + node.tail
            else: # The parent get the tail as text
                parent = node.getparent()
                if parent.text is None:
                    if node.tag == '{urn:oasis:names:tc:xliff:document:1.2}x':
                        if node.attrib['ctype'] == 'x-tab':
                            equiv_text = '\t'
                        if node.attrib['ctype'] == 'lb':
                            equiv_text = '\t'
                        parent.text = equiv_text + node.tail
                    else:
                        parent.text = node.tail
                else:
                    if node.tag == '{urn:oasis:names:tc:xliff:document:1.2}x':
                        equiv_text = ''
                        if node.attrib['ctype'] == 'x-tab':
                            equiv_text = '\t'
                        if node.attrib['ctype'] == 'lb':
                            equiv_text = '\t'
                        parent.text = parent.text +equiv_text + node.tail
                    else:
                        parent.text = parent.text + node.tail
                    
    def remove_keeping_text_tail(self, element):
        """Safe the tail text and then delete the element"""
        # Solution from Stackoverflow:
        # https://stackoverflow.com/questions/42932828/how-delete-tag-from-node-in-lxml-without-tail
        self._preserve_tail_text_before_delete(element)
        element.getparent().remove(element)
    
    def __text_visualize(self, delete_text: bool=False, replace_tag: bool=True):
        """
        Internal method to replace text
        """
        xml_temp = deepcopy(self.xml)
        tags = [node.tag for node in xml_temp]
        for node in xml_temp:
            if self.wf_version == 'txlf':
                if (node.tag == '{urn:oasis:names:tc:xliff:document:1.2}x' or
                    node.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph' or
                    node.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'):
                    if node.attrib['ctype'] == 'x-tab':
                        node.text = '\t'
                    elif node.attrib['ctype'] == 'lb':
                        node.text = '\n'
                    elif ((node.attrib['ctype'].lower() == 'x-fontformat' 
                     or node.attrib['ctype'] == 'x-shape')
                     and node.text):
                        node.text = ''
                    elif  node.attrib['ctype'].lower() == 'x-linefeed' and node.text:
                        node.text = '\n'
                elif node.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept':
                    node.text = ''
            else:
                if node.tag == 'ut':
                    if node.attrib['type'] == 'fontformat' or 'fontformat' in node.text:
                        node.text = ''
        if delete_text:
            if replace_tag:
                temp_text = xml_temp.text if xml_temp.text else ''
                for node in xml_temp:
                    temp_text += '{tag}'
                    temp_text += node.tail if node.tail else ''
                    xml_temp.remove(node)
                xml_temp.text = temp_text
            else:
                et.strip_elements(xml_temp, *tags, with_tail=False)
        else:
            et.strip_tags(xml_temp, *tags)
        return xml_temp.text

    def _preserve_tail_text_before_delete(self, node):
        """
        Includes in the contents of the text the contents of its tags.
        """
        if node.tail: # preserve the tail
            previous = node.getprevious()
            node_text = ''
            if self.wf_version == 'txlf':
                if (node.tag == '{urn:oasis:names:tc:xliff:document:1.2}x' or
                    node.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph' or
                    node.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'):
                    if node.attrib['ctype'] == 'x-tab':
                        node_text = '\t'
                    elif node.attrib['ctype'] == 'lb':
                        node_text = '\n'
                    elif ((node.attrib['ctype'].lower() == 'x-fontformat' 
                     or node.attrib['ctype'] == 'x-shape')
                     and node.text):
                        node_text = ''
                    elif  node.attrib['ctype'].lower() == 'x-linefeed' and node.text:
                        node_text = '\n'
                    elif node.get('ctype').lower() == 'x-unknown' and node.text:
                        node_text = node.text
                elif node.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept':
                    node_text = ''
                else:
                    node_text = node.text
            else:
                if (node.tag == 'ut'):
                    if node.attrib['type'] == 'fontformat' or 'fontformat' in node.text:
                        node_text = ''
                    else:
                        node_text = node_text
            if previous is not None: # if there is a previous sibling it will get the tail
                if previous.text is None:
                    previous.text = node_text
                else:
                    previous.text = previous.text + node_text
                if previous.tail is None:
                    previous.tail = node.tail
                else:
                    previous.tail = previous.tail + node.tail
            else: # The parent get the tail as text
                parent = node.getparent()
                if parent.text is None:
                    if node_text:
                        parent.text = node_text + node.tail
                    else:
                        parent.text = node.tail
                else:
                    try:
                        parent.text = parent.text + node_text + node.tail
                    except TypeError:
                        if node_text and node.tail is None:
                            parent.text = parent.text + node_text
                        elif node_text is None and node.tail:
                            parent.text = parent.text + node.tail
    
    def get_tags(self):
        """
        Read the tags of the content.
        Store the tags in the attribute in the key-value tags dictionary.
        Store the id of the tag as the key and the text as the value.
        """
        xml_temp = et.fromstring(self.xml_string)
        self.tags = dict()
        for child in xml_temp.iterchildren():
            if self.wf_version == 'txlf':
                if (child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept' 
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ex'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bx'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}x'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}sub'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}it'):
                        tag_id = child.attrib['id']
                        self.tags[tag_id] = child.text
            else:
                if child.tag == 'ut':
                    tag_id = child.attrib['x']
                    self.tags[tag_id] = child.text
    
    def replace_tags(self, orig: str, repl: str, regex: bool=False):
        """
        Replace the text of a tag for another value.
        
        """
        for child in self._xml.iterchildren():
            if self.wf_version == 'txlf':
                if (child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept' 
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ex'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bx'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}x'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}sub'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}it'):
                        if child.text == orig:
                            child.text = repl
            else:
                if child.tag == 'ut':
                    if child.text == orig:
                        child.text = repl
    
    def replace_text(self, text: str, repl: str):
        """
        Replace the text with a value. 
        """
        if self.xml_text.text:
            self.xml_text.text = str(self.xml_text.text).replace(text, repl)
        for elem in self.xml_text:
            if elem.tail:
                elem.tail = str(elem.tail).replace(text, repl)
    
    def __regex_replace_text_elem(self, tparent: et.Element, trepl: str, tsearch_pat: object, 
        attrib: dict={'id': '1', 'ctype': 'x-regex'}, tail: bool=False):
        """
        """
        if tail:
            text = tparent.tail
        else:
            text = tparent.text
        if text:
            match = tsearch_pat.search(text)
            if match:
                tag = et.SubElement(tparent, trepl, attrib=attrib)
                if tag.get('ctype') == 'x-regex':
                    text = match.group(0)
                    if tail:
                        tparent.tail = tsearch_pat.sub('', text)
                    else:
                        tparent.text = tsearch_pat.sub('', text)
                    tag.text = f'<regex captured="{text}">'
            
    def replace_text_by_tag(self, tsearch: str,
                 tag: str='{urn:oasis:names:tc:xliff:document:1.2}ph',
                 ctype: str='x-regex', 
                 regex: bool=True):
        """
        Replace the text by a tag
        """
        if regex:
            tsearch_pat = re.compile(tsearch)
        if len(self._xml) == 0:
            if regex:
                self.__regex_replace_text_elem(self._xml, tag, tsearch_pat, {'id': '1', 'ctype': ctype})
        else:
            n = len(self._xml)
            for elem in self._xml.iter():
                if 'source' in elem.tag or 'target' in elem.tag:
                    if elem.tail:
                        if tsearch_pat.search(elem.tail):
                            n += 1
                            self.__regex_replace_text_elem(
                                                                        elem,tag,
                                                                        tsearch_pat,
                                                                        {
                                                                            'id': f'{n}',
                                                                            'ctype': ctype
                                                                        },
                                                                        True)
                    if elem.text:
                        if tsearch_pat.search(elem.tail):
                            n += 1
                            self.__regex_replace_text_elem(
                                                                        elem,tag,
                                                                        tsearch_pat,
                                                                        {
                                                                            'id': f'{n}',
                                                                            'ctype': ctype
                                                                        })
    
    def replace_tag(self, tag: et.Element, orig: str, repl: str) -> int:
        """
        Replace a tag
        """
        if tag.text == orig:
            tag.text = repl
            return 0
        else:
            return -1
    
    def yield_tags(self):
        """
        """
        for child in self._xml.iterchildren():
            if self.wf_version == 'txlf':
                if (child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept' 
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ex'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bx'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}x'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}sub'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}it'):
                        yield child
            else:
                if child.tag == 'ut':
                    yield child
    
    def find_tail(self, char: str) -> dict:
        """
        Check if the tail is the specified character or string. It returns a dictionary with
        the id as key and the tail as value.
        
        Arguments: 
            char (str): Character or string to check.
        
        Return:
            tags (dict): Return a dictionary with the id as key and the tail text as value.
        """
        xml_temp = et.fromstring(self.xml_string)
        tags = dict()
        for child in xml_temp.iterchildren():
            if self.wf_version == 'txlf':
                if (child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept' 
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ex'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bx'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}x'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}sub'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}it'):
                        tag_id = child.attrib['id']
                        if child.tail == char:
                            tags[tag_id] = char
            else:
                if child.tag == 'ut':
                    tag_id = child.attrib['x']
                    tags[tag_id] = char
        return tags
    
    def replace_tail(self, tags: dict) -> None:
        """
        Replace the specified tags with the indicated values in the dictionary tags.
        Return nothing.
        
        Arguments:
            tags (dict): Dictionary of the id of the tag and the tail value.
        
        """
        #xml_temp = et.fromstring(self.xml_string)
        for id, value in tags.items():
            for child in self._xml.iterchildren():
                if self.wf_version == 'txlf':
                    if (child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept' 
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ex'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bx'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}x'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}sub'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}it'):
                        if child.attrib['id'] == id:
                                child.tail = value
    
    def __nodes_browse(self, delete=True):
        """
        """
        xml_temp = et.fromstring(self.xml_string)
        for child in xml_temp.iterchildren():
            if self.wf_version == 'txlf':
                if (child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept' 
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ex'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bx'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}x'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}sub'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}it'):
                        if delete:
                            self.remove_keeping_tail(child)
                        else:
                            self.replace_tag_keeping_tail(child)
            else:
                if child.tag == 'ut':
                    if delete:
                        self.remove_keeping_tail(child)
                    else:
                        self.replace_tag_keeping_tail(child)
        return xml_temp
    
    def __nodes_browse_text(self):
        """
        """
        
        xml_temp = et.fromstring(self.xml_string)
        for child in xml_temp.iterchildren():
            if self.wf_version == 'txlf':
                if (child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ept' 
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bpt'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ph'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}ex'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}bx'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}x'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}sub'
                    or child.tag == '{urn:oasis:names:tc:xliff:document:1.2}it'):
                        
                        self.remove_keeping_text_tail(child)
                        
            else:
                if child.tag == 'ut':
                    self.remove_keeping_text_tail(child)
        return xml_temp
    
    # Method upgraded to the new structure.
    def find_terms(self, pattern_text: str,  excluding_word: str=''):
        """
        Looks for a pattern and finds this text based on an attribute.
        Also it allows to exclude a word. Similar to the method of segment.
        But it only checks in the property only_text.
        Arguments:
            pattern_text (str): The pattern to be checked.
            excluding_word(str): Word that matches with the pattern. But
                        should not be included.
        """
        pattern = re.compile(pattern_text)
        items_found =  pattern.findall(self.only_text)
        if excluding_word:
            self.terms = [term for term in items_found if term != excluding_word]
        else:
            self.terms = items_found
        return self.terms

    def flash_find_terms(self, keywords, attribute):
        """
        Looks for the specified keywords. It is a faster alternative to find_terms
        when the whole word is needed to searched.
        Arguments:
            keywords: String or tuple or list of strings containing the keywords to search
            attribute: Where has to be looked for the keyword.
        """
        from flashtext.keyword import KeyWordProcessor
        keyword_processor = KeyWordProcessor()
        if isinstance(keywords, str):
            keyword_processor.add_keyword(keywords)
        if isinstance(keywords, (list, tuple)):
            for keyword in keywords:
                keyword_processor.add_keyword(keyword)
        if attribute:
            self.terms = keyword_processor.extract_keywords(attribute)
        return self.terms
    
    def check_pattern(self, pattern, mode):
        """
        Checks if the text follows the pattern. And return True or False.
        Arguments:
            pattern(str): Regex expression to be checked
            mode(int): The mode that has to be done the check. 
                            0: Only matches in the beginning of the pattern
                            1: Searchs the string pattern in the whole string
        """
        pat_obj = re.compile(pattern)
        if mode == 0:
            if pat_obj.match(self.only_text):
                return True
            else: 
                return False
        elif mode == 1:
            if pat_obj.search(self.only_text):
                return True
            else:
                return False

    def paste_segment_content(self, segcontent: object, overwrite: bool=True):
        """
        Paste a segment content into the actual segment content. It copies only its contents.
        It is useful when you need to swap contents for example.
        
        Arguments:
            segcontent (SegmentContent): The SegmentContent to be pasted (source or target)
            overwrite (bool): Overwrite the current contents or paste the new ones at the end, merge.
        """
        tgt_elem = self.xml
        if segcontent.wf_version == self.wf_version:
            if overwrite:
                if tgt_elem is not None:
                    for elem in tgt_elem.iterchildren():
                        tgt_elem.remove(elem)
                    tgt_elem.text = segcontent.xml.text
            else:
                self.convert_ws_to_string()
                print('First paste xml:', self.xml)
                if len(tgt_elem) > 0:
                    if tgt_elem[-1].tail:
                        tgt_elem[-1].tail += segcontent.xml.text
                    else:
                        tgt_elem[-1].tail = segcontent.xml.text
                else:
                    #print(tgt_elem, tgt_elem.text)
                    # Keep in vigilance this as possible source to errors.
                    try:
                        tgt_elem.text += segcontent.xml.text
                    except AttributeError:
                        return
            if len(segcontent.xml) > 0:
                segcontentxml = deepcopy(segcontent.xml)
                for elem in segcontentxml:
                    print(f'Adding {elem}')
                    tgt_elem.append(elem)
        else:
            raise TypeError('The type of the SegmentContent is not the same. '
                                 'It must be both TXLF or TXML.')

    def jsonify(self):
        """
        Convert the object into a JSON object
        """
        if self.xml_text is not None and not isinstance(self.xml_text, str):
            xml_text = et.tostring(self._xml,encoding=str)
        else:
            xml_text = ''
        return {
            'attributes' : self.attributes, 
            'wf_version': self.wf_version, 
            'all_text': self.all_text,
            'only_text': self.only_text,
            'target_score': self.target_score, 
            'xml_text': xml_text
            }

class Revision:
    """
    Handle the revisions of a segment.
    It is a class that should not be directly instantiated.
    Use it in segment.revisions.
    Example:
        file_wf = Wf('c:\wf_file\wf_file.txlf')
        for paragraph in file_wf.paragraphs:
            for segment in paragraph.segments:
                print(len(segment.revisions)) # Show number of revisions
    """
    
    def __init__(self, xml_revision, wf_version):
        """
        Initialitzer of Revision. It initialitzes the object
        when it is created/instantatied with the contents of 
        the xml of <revisions> or <alt-trans> and it inherites from its
        parent (Segment) the wf_version that is necessary to handle
        correctly the file, depending if it is TXLF or TXML.
        """
        self.xml_revision = xml_revision
        self.wf_version = wf_version
        # Parents
        self.segment = None
        self.paragraph = None
        self.wf_file = None
        # Other initializations
        self._add_target()
        self.__attributes = dict()
        self._notes = []
        self._xml_notes = None
        self.__load_notes()
        
    def _add_target(self):
        """
        Internal method that set the value of target.
        Add a SegmentContent object that reads the content of <target>
        in <revisions> or <alt-trans>.
        """
        self.target = SegmentContent('target', self.xml_revision, self.wf_version)
    
    def __load_notes(self):
        if self.wf_version == 'txlf':
            self._xml_notes = self.xml_revision.xpath('gs4tr:note', namespaces=txlf_nsmap)
        if self._xml_notes:
            self._notes = [xml_note.text for xml_note in self.xml_notes]
    
    @property
    def notes(self):
        """
        Get the notes from revisions
        """
        return self._notes
    
    @property
    def xml_notes(self):
        return self._xml_notes
    
    @property
    def attributes(self):
        """
        Get all the attributes of the revision section.
        It will return the raw attributes and not formatted.
        """
        if not self.__attributes:
            for attribute, value in self.xml_revision.attrib.items():
                self.__attributes[attribute] = value
        return self.__attributes
    
    def get_attribute(self, attribute):
        """
        Get the value of the indicated attribute.
        Arguments:
            attribute (str): Name of the attribute to retrieve.
        Return:
            value(str): The value of the indicated attribute.
            If it is empty or does not exist return None.
        """
        if not self.__attributes and self._xml:
            for attr, value in self._xml.attrib:
                if attr == attribute:
                    return value
        elif self.__atributes:
            for attr,  value in self.__attributes:
                if attr == attribute:
                    return value
        elif not self.__attributes and not self._xml:
            return None
    
    def set_attribute(self, attribute, value):
        """
        Add or modidfy an existing attribute.
        Warning: If an attribute needs to be added to a TXLF file,
        it is important to know if the attribute has namespaces, 
        for example gs4tr:score.
        The namespaces for gs4tr would be {http://www.gs4tr.org/schema/xliff-ext}.
        So to add the attribute score in a TXLF file would be done as 
        the following example:
        
        Example:
            set_attribute('{http://www.gs4tr.org/schema/xliff-ext}score', '100')
        
        Arguments:
            attribute (str): Attribute to modify or add.
        """
        self.__attributes[attribute] = value
        if self._xml:
            self._xml.attrib[attribute] = value


class Grade:
    """
    Grade of a review
    """
    def __init__(self, xml_grade: et.Element, wf_version: str, parent: object=None):
        self._xml = xml_grade
        self.wf_version = wf_version
        self._parent = parent
        self._id = None
        self._grade = None
        self._score = None
        self.__initialize()
        
    def __initialize(self) -> None:
        if self._xml is not None:
            self._grade = self._xml.get('grade')
            self._id = self._xml.get('gId')
            self._score = self._xml.get('score')
    
    def delete(self):
        """
        """
        self._parent.grades.remove(self)
        self._xml.getparent().remove(self._xml)
    
    @property
    def parent(self):
        return self._parent
    
    @property
    def id(self):
        """
        """
        if not self._id:
            self._id = self._xml.get('gId')
        return self._id
    
    @property
    def grade(self):
        """
        """
        if not self._grade:
            self._grade = self._xml.get('grade')
        return self._grade
    
    @grade.setter
    def grade(self,  value: str):
        if isinstance(value, str):
            self._grade = value
            self._xml.set('grade', value)
        else:
            raise TypeError(f'The score must be "str" and it is {type(value)}')
            
    
    @property
    def score(self):
        """
        """
        if not self._score:
            self._score = self._xml.get('score')
        return self._score

    @score.setter
    def score(self,  value: str):
        if isinstance(value, str):
            self._score = value
            self._xml.set('score', value)
        else:
            raise TypeError(f'The score must be "str" and it is {type(value)}')


class Note:
    """
    Note element
    """
    def __init__(self, xml: str, wf_version: str='txlf', text: str= '', parent: object=None):
        """
        """
        self._xml = xml
        self._parent = parent
        self._wf_version = wf_version
        self._text = text
        self._type = ''
        self._id = None
    
    @property
    def text(self):
        """
        """
        return self._text
    
    @text.setter
    def text(self, value):
        if isinstance(value, str):
            self._text = value
            self._xml.text = value
        else:
            raise TypeError(f'The text of the note must be <str> and it is {type(value)}')
    
    @property
    def parent(self):
        """
        """
        return self._parent
    
    @property
    def xml(self):
        return self._xml
    
    @property
    def type(self):
        """
        """
        if self._xml:
            self._type = self._xml.get('segment-note-type')
        return self._type
    
    @property
    def id(self):
        if self._xml:
            self._id = self._xml.get('snid')


class ReviewInfo:
    """
    Class that represents the information of Review (element gs4tr:review).
    Only TXLF.
    """
    def __init__(self, xml_review: et.Element, wf_version: str, parent: object=None):
        """
        Initialize the object ReviewInfo
        """
        self._xml = xml_review
        self._parent = parent
        self._wf_version = wf_version
        self._user = None
        self._timestamp = None
        self._tool_name = None
        self._tool_version = None
        self._grades = self.__load_grades()
        self._notes = self.__load_notes()
    
    def __load_grades(self):
        grades = self._xml.xpath('gs4tr:grade',  namespaces=txlf_nsmap)
        return [Grade(grade, self._wf_version, self) for grade in grades]
    
    def __load_notes(self):
        return [Note(note, self.wf_version, note.text, self) for note in self._xml.xpath(
                    'gs4tr:note', namespaces=txlf_nsmap)]
    
    @property
    def xml(self):
        return self._xml
    
    @property
    def parent(self):
        return self._parent
    
    @property
    def wf_version(self):
        return self._wf_version
    
    @property
    def user(self):
        if not self._user:
            self._user = self.xml.get('from')
        return self._user
    
    @user.setter
    def user(self, value: str):
        if isinstance(value, str):
            self._user = value
            self.xml.set('from', value)
     
    @property
    def timestamp(self):
        if not self._timestamp:
            self._timestamp = self.xml.get('timestamp')
        return self._timestamp
    
    @timestamp.setter
    def timestamp(self, value: str):
        if isinstance(value, str):
            self._user = value
            self.xml.set('from', value)
    
    @property
    def tool_name(self):
        if not self._tool_name:
            self._tool_name = self.xml.get('tool-name')
        return self._tool_name
    
    @tool_name.setter
    def tool_name(self, value: str):
        if isinstance(value, str):
            self._user = value
            self.xml.set('tool-name', value)
     
    @property
    def tool_version(self):
        if not self._tool_version:
            self._tool_version = self.xml.get('tool-version')
        return self._tool_version
    
    @tool_version.setter
    def tool_version(self, value: str):
        if isinstance(value, str):
            self._user = value
            self.xml.set('from', value)
     
    @property
    def grades(self):
        return self._grades
    
    @property
    def notes(self):
        return self._notes
    
    def delete_grades(self):
        """
        """
        self._grades = []
        for grade in self._xml.xpath('gs4tr:grade', namespaces=txlf_nsmap):
            grade.getparent().remove(grade)
    
    def delete_notes(self):
        """
        """
        self._notes = []
        for note in self._xml.xpath('gs4tr:note', namespaces=txlf_nsmap):
            note.getparent().remove(note)
    
    def delete_note(self, snid: int=0):
        """
        """
        for note in self._notes:
            if note.id == snid:
                note._xml.getparent().remove(note._xml)
                self._notes.remove(note)
    
    def add_grade(self, score: str, grade: str):
        """
        Add a grade to the review.
        
        Arguments:
            score (str): The severity of the error is inserted here.
            grade (str): What type of error was done.
        """
        grade_xml = et.SubElement(self._xml, '{http://www.gs4tr.org/schema/xliff-ext}grade')
        id = len(self.grades)
        grade_xml.attrib['gId'] = str(id)
        grade_xml.attrib['score'] = str(score)
        grade_xml.attrib['grade'] = grade
        self._grades.append(Grade(grade_xml, self.wf_version, self))

    def add_note(self, value: str, note_type: str='segment-note-type'):
        """
        Add a note to the review
        """
        note_xml = et.SubElement(self._xml, '{http://www.gs4tr.org/schema/xliff-ext}note')
        id = len(self.notes)
        note_xml.set('snid', str(id)) 
        note_xml.set('segment-note-type', note_type)
        note_xml.text = str(value)
        self._notes.append(Note(note_xml, self.wf_version, parent=self))


class Segment:
    """
    Represent the segment part of the bilingual file. 
    There is stored the source and the target content.
    This class hass not be directly instantiated. Instead use the Paragraph.segments.
    
    Example:
        wf_file = Wf('c:\file\wf_file.txlf')
        for paragraph in wf_file.paragraphs:
            print(len(paragraph.segements)) # Total of segments of the paragraph
    
    Attributes:
        source (dict): Contains the information of the source. It has the following keys:
            text (str): It shows the full text, indicating the position of the tags as {tag}.
            text_only (str): It removes all tags only showing the actual content.
            xml (Element): It shows the whole xml code of the source element.
            xml_string (str): The xml in string format.
        target (dict): Target information. Has the same structure as source.
        paragraph (Paragraph): reference to the parent Paragraph.
        wf (Wf): reference to Wf object.
        terms (list): List of terms that has been looked for specific purposes.
    """
    def __init__(self, xml_segment, wf_version, hidden: bool=False, par_position: int=1):
        """
        Initialization of the class Segment.
        
        Example:
            segment = Segment(xml_segment, 'txlf')
            
        Arguments:
            xml_segment (Element): <segment> XML element of the Wf file.
            wf_version (str): Version of Wf. Can be txlf or txml.
            hidden (bool): Indicates if the segment is in translate status or not
            par_position (int): Indicates the position of the segment in the paragraph
        """

        self.xml_seg = xml_segment
        self.wf_version = wf_version
        
        self.__id = ''
        self.terms = []
        self._add_source()
        self._add_target()
        self.__number = 1
        self.__notes = []
        self.__maxlen = None
        self.__locked = False
        self.__found_in_preview = False
        self.__translate = True
        self.__attributes = []
        self.paragraph = None
        self.wf = None
        self._revisions = []
        self._xml_revisions = None
        self._hidden = hidden
        self.xml_comment = None
        self._paragraph_position = par_position
        self._reviews = []
        self.load_review_info()
    
    @property
    def xml(self):
        """
        XML of the segment
        """
        return self.xml_seg
    
    @xml.setter
    def xml(self, value: et.Element):
        if isinstance(value, et.Element):
            self.xml_seg = value
        else:
            raise TypeError('It should be an etree.Element and it is another type.')
    
    
    @property
    def seg_number(self):
        """
        Number of the segment in the file. It is independent to the id of the segment
        in a paragraph. It is the value that would be shown in the visual tool like 
        WordFast, TSO or TSR.
        """
        return self.__number
    
    @seg_number.setter
    def seg_number(self, value):
        self.__number = value
    
    @property
    def paragraph_position(self):
        """
        Return the position of the segment in the paragraph.
        """
        return self._paragraph_position
    
    @property
    def id(self):
        """
        Identifier/number of the segment in the paragraph.
        """
        if self.wf_version == 'txlf':
            
            self.__id = self.xml_seg.attrib['id']
        else:
            self.__id = self.xml_seg.attrib['segmentId']
        return self.__id
    
    @property
    def attributes(self):
        """
        Get all the attributes of the paragraph.
        It will return the raw attributes and not formatted.
        """
        if not self.__attributes:
            for attribute, value in self.xml_seg.attrib.items():
                self.__attributes[attribute] = value
        return self.__attributes
    
    @property
    def revisions(self):
        return self._revisions
    
    @property
    def revision_notes(self):
        """
        Read the revision notes of the segment
        """
        if self.wf_version == 'txlf':
            xml_notes = self.xml_seg.xpath('gs4tr:review/gs4tr:note',  namespaces=txlf_nsmap)
        if xml_notes:
            return [note.text for note in xml_notes]
    
    @property
    def notes(self):
        """
        Get the notes. Only 1 note can be set.
        TODO:
            Change the property to read only and change the setter to
                         a full method allowing to add more notes.
        """
        if self.wf.wf_file:
            if self.wf_version == 'txlf':
                self.xml_notes = self.xml_seg.xpath('gs4tr:note', namespaces=txlf_nsmap)
                
            else:
                self.xml_notes = self.xml_seg.xpath('./comments/comment')
            if self.xml_notes:
                self.__notes = [xml_notes.text for xml_notes in self.xml_notes]
            else: self.__notes = []
        return self.__notes
    
    def add_review(self,
                            user: str,
                            timestamp: str=datetime.utcnow().strftime("%Y%m%dT%H%M%SZ"),
                            tool_name: str='TSR', 
                            tool_version: str='6.0.3', 
                            phase_name: str='Review-1'
                            ):
        """
        Add a review object
        Arguments:
            user (str): The username to be added, matching with the from
            timestamp (str): Date of creation of the Review
        """
        xml_review = et.SubElement(self.xml, '{http://www.gs4tr.org/schema/xliff-ext}review')
        xml_review.set('from', user)
        xml_review.set('timestamp', timestamp)
        xml_review.set('tool-name', tool_name)
        xml_review.set('tool-version', tool_version)
        xml_review.set('phase-name', phase_name)
        review = ReviewInfo(xml_review, self.wf_version, self)
        self.reviews.append(review)
    
    def add_note(self,
                         value: str,
                         num_note: int=0,
                         username='',
                         date_creation: str=datetime.utcnow().strftime("%Y%m%dT%H%M%SZ"),
                         type: str='translation') -> None:
        """
        Adds note allowing more flexibility than the notes setter making it deprecated 
        and obsolete and to remove in next versions.
        Arguments:
            value (str): Adds the text to the note.
            num_note (str): Number of the note.
            username (str): User name that created the note.
            date_creation (str): Date string of when the note was created
            type (str): Identifies what type of note it was. For example, translation, proofreading...
        Return:
            None
        """
        if value:
            self.__notes.append(value)
            if not username:
                try:
                    username = getlogin()
                except OSError as e:
                    if 'errno 6' in str(e):
                        username = 'tpt_generic'
            if self.wf.wf_file:
                user_name = username
                if self.wf_version == 'txlf':
                    if num_note == 0:
                        if not self.notes:
                            note_num = num_note
                        else:
                            note_num = len(self.notes)
                    else:
                        note_num = num_note
                    creation_date = date_creation
                    segment_note_type = type 
                    note_sub_elem = et.SubElement(self.xml_seg,
                     '{http://www.gs4tr.org/schema/xliff-ext}note')
                    note_sub_elem.attrib['snid'] = str(note_num)
                    note_sub_elem.attrib['from'] = user_name
                    note_sub_elem.attrib['timestamp'] = creation_date
                    note_sub_elem.attrib['segment-note-type'] = segment_note_type
                else:
                    creation_date = date_creation
                    if type == 'translation':
                       segment_note_type = 'text'
                    else:
                       segment_note_type = type 
                    if self.notes is None:
                        notes_sub_elem = et.SubElement(self.xml_seg, 'comments')
                    else:
                        try:
                            notes_sub_elem = self.xml_seg.xpath('./comments')[0]
                        except IndexError:
                            notes_sub_elem = et.SubElement(self.xml_seg, 'comments')
                    note_sub_elem = et.SubElement(notes_sub_elem, 'comment')
                    note_sub_elem.attrib['creationid'] = user_name
                    note_sub_elem.attrib['creationdate'] = creation_date
                    note_sub_elem.attrib['type'] = segment_note_type
                note_sub_elem.text = value
   
    @notes.setter
    def notes(self, value):
        if value:
            self.__notes.append(value)
            
            if self.wf.wf_file:
                user_name = getlogin()
                
                if self.wf_version == 'txlf':
                    if len(self.notes) == 0:
                        note_num = 0
                    else:
                        note_num = len(self.notes)
                    creation_date = datetime.utcnow().strftime("%Y%m%dT%H%M%SZ")
                    note_sub_elem = et.SubElement(self.xml_seg,
                     '{http://www.gs4tr.org/schema/xliff-ext}note')
                    note_sub_elem.attrib['snid'] = str(note_num)
                    note_sub_elem.attrib['from'] = user_name
                    note_sub_elem.attrib['timestamp'] = creation_date
                    note_sub_elem.attrib['segment-note-type'] = 'translation'
                    
                else:
                    creation_date = datetime.utcnow().strftime("%Y%m%dT%H%M%SZ")
                    if self.notes is None:
                        notes_sub_elem = et.SubElement(self.xml_seg, 'comments')
                    else:
                        try:
                            notes_sub_elem = self.xml_seg.xpath('./comments')[0]
                        except IndexError:
                            notes_sub_elem = et.SubElement(self.xml_seg, 'comments')
                    note_sub_elem = et.SubElement(notes_sub_elem, 'comment')
                    note_sub_elem.attrib['creationid'] = user_name
                    note_sub_elem.attrib['creationdate'] = creation_date
                    note_sub_elem.attrib['type'] = 'text'
                #print('The value to be inserted in the note: ', value)
                note_sub_elem.text = value
                #print('The text of the note: ', note_sub_elem.text)
                
        elif not value or value == '':
            self.__notes = []
            if self.wf.wf_file:
                if self.wf_version == 'txlf':
                    try:
                        for note in self.xml_seg.findall('gs4tr:note', namespaces=txlf_nsmap):
                            self.xml_seg.remove(note)
                    except:
                        pass
                else:
                    try:
                        self.xml_seg.remove(self.xml_seg.find('comments'))
                    except:
                        pass
                    
        #self.wf.update_root(et.ElementTree(self.wf.xml_tree.getroot())) 

    @property
    def maxlen(self):
        """
        Handles the maxlen of a paragraph. Get and set the maxlen of a paragraph.
        """
        if not self.__maxlen and Path(self.wf.wf_file).is_file():
            try:
                if self.wf_version == 'txlf':
                    self.__maxlen = self.xml_seg.attrib[
                             '{http://www.gs4tr.org/schema/xliff-ext}maxlen']
                else:
                    self.__maxlen = self.xml_seg.attrib['maxlen']
            except KeyError:
                self.__maxlen = None
        return self.__maxlen
        
    @maxlen.setter
    def maxlen(self, value):
        self.__maxlen = value
        if Path(self.wf.wf_file).is_file():
            if self.wf_version == 'txlf':
                if value:
                    self.xml_seg.attrib[
                                 '{http://www.gs4tr.org/schema/xliff-ext}maxlen'
                                 ] = self.__maxlen
                else:
                    try:
                        self.xml_seg.attrib.pop('{http://www.gs4tr.org/schema/xliff-ext}maxlen')
                    except KeyError:
                        pass
            else:
                if value:
                    self.xml_seg.attrib['maxlen'] = self.__maxlen
                else:
                    try:
                        self.xml_seg.attrib.pop('maxlen')
                    except KeyError:
                        pass
    
    @property
    def found_in_preview(self):
        """
        Check if a segment is found in preview.
        It is a property mainly found in e-learning projects.
        """
        if self.wf_version == 'txlf':
            try:
                self.__found_in_preview = self.xml_seg.attrib.get(
                                '{http://www.gs4tr.org/schema/xliff-ext}foundInPreview')
            except KeyError:
                self.__found_in_preview = True
        return self.__found_in_preview

    @found_in_preview.setter
    def found_in_preview(self, value: bool):
        if self.wf_version == 'txlf':
            self.__found_in_preview = value
            if value:
                try:
                    self.xml_seg.attrib[
                                    '{http://www.gs4tr.org/schema/xliff-ext}foundInPreview'] = 'true'
                except KeyError:
                    pass
            else:
                self.xml_seg.attrib[
                                    '{http://www.gs4tr.org/schema/xliff-ext}foundInPreview'] = 'false'

    @property
    def locked(self):
        """
        Checks if the segment is locked. It is a property only valid for TXLF files.
        """
        if not self.__locked and self.wf_version == 'txlf':
            try:
                self.__locked = self.xml_seg.attrib.get(
                                        '{http://www.gs4tr.org/schema/xliff-ext}locked',
                                         False)
            except KeyError:
                self.__locked = False
        elif self.wf_version == 'txml':
            raise AttributeError('This property is only valid for wf_version=TXLF')
        return self.__locked

    @locked.setter
    def locked(self, value: bool):
        self.__locked = value
        if self.wf_version == 'txlf':
            if value:
                self.xml_seg.attrib[
                        '{http://www.gs4tr.org/schema/xliff-ext}locked'] = 'true'
            else:
                try:
                    self.xml_seg.attrib.pop('{http://www.gs4tr.org/schema/xliff-ext}locked')
                except KeyError:
                    pass
        else:
            raise AttributeError('This property is only valid for wf_version=TXLF')
    
    @property
    def wordcount(self) -> int:
        """
        Return the value of the attribute wordcount. It exists only if it has been leveraged.
        Status: Implemented for TXLF only.
        """
        wordcount = self.xml_seg.get('{http://www.gs4tr.org/schema/xliff-ext}wordcount')
        if wordcount:
            return int(wordcount)
        else:
            return None
    
    @property
    def hidden(self):
        """
        Check if the segment is for translation or not. It sets hidden attribute in TXLF files.
        This implementation is for future TXLF versions
        """
        if self.wf_version == 'txlf':
            try:
                self._hidden = False if self.xml_seg.attrib.get(
                    '{http://www.gs4tr.org/schema/xliff-ext}hidden', 'false') == 'false' else True
            except KeyError:
                self._hidden = True
        return self._hidden

    @hidden.setter
    def hidden(self, value: bool):
        self._hidden = value
        if self.wf_version == 'txlf' and (
          value is True or value is False):
            self.xml_seg.attrib[
                '{http://www.gs4tr.org/schema/xliff-ext}hidden'] = 'true' \
                    if value == True else 'false'
            if self.xml_seg.attrib.get(
                  '{http://www.gs4tr.org/schema/xliff-ext}hidden') == 'false':
                    self.xml_seg.attrib.pop('{http://www.gs4tr.org/schema/xliff-ext}hidden')

    def _add_source(self):
        """
        Internal method that sets the values of the source
        It is used to generate the source of the segment when Segment class
        is initialitzed.
        """
        #self.source = self.__add_tgt_src_values('source')
        self.source = SegmentContent('source', self.xml_seg, self.wf_version)
    
    def _add_target(self):
        """
        Internal method to set the values of target.
        It is used to generate the target of the segment when Segment class
        is initialitzed.
        """
        #self.target = self.__add_tgt_src_values('target')
        self.target = SegmentContent('target', self.xml_seg, self.wf_version)
    
    def find_terms(self, pattern_text, attribute, excluding_word=''):
        """
        Looks for a pattern and finds this text based on an attribute.
        Also it allows to exclude a word.
        """
        pattern = re.compile(pattern_text)
        if attribute:
            items_found =  pattern.findall(attribute)
            if excluding_word:
                self.terms = [term for term in items_found if term != excluding_word]
            else:
                self.terms = items_found
        return self.terms
    
    def flash_find_terms(self, keywords, attribute):
        """
        Looks for the specified keywords. It is a faster alternative to find_terms
        when the whole word is needed to searched.
        Arguments:
            keywords: String or tuple or list of strings containing the keywords to search
            attribute: Where has to be looked for the keyword.
        """
        from flashtext.keyword import KeyWordProcessor
        keyword_processor = KeyWordProcessor()
        if isinstance(keywords, str):
            keyword_processor.add_keyword(keywords)
        if isinstance(keywords, (list, tuple)):
            for keyword in keywords:
                keyword_processor.add_keyword(keyword)
        if attribute:
            self.terms = keyword_processor.extract_keywords(attribute)
        return self.terms
        
    def load_revisions(self):
        """
        Load the revisions that are present in the segment.
        """
        if self.xml_seg is not None:
            if self.wf_version == 'txlf':
                xml_revisions = self.xml_seg.xpath('Default:alt-trans', namespaces=txlf_nsmap)
                self._revisions = [Revision(xml_revision, self.wf_version
                              ) for xml_revision in xml_revisions]
            else:
                xml_revisions = self.xml_seg.xpath('revisions')
                self._revisions = [Revision(xml_revision, self.wf_version
                                  ) for xml_revision in xml_revisions]
    
    def load_review_info(self):
        """
        Read the items gs4tr:review from the segment.
        """
        if self.wf_version == 'txlf':
            if self.xml_seg is not None:
                xml_reviews = self.xml_seg.xpath('gs4tr:review',  namespaces=txlf_nsmap)
                for xml_review in xml_reviews:
                    self._reviews.append(ReviewInfo(xml_review, self.wf_version, self))
                    
    @property
    def reviews(self):
        """
        Return the reviews of the file
        """
        return self._reviews
    
    def delete_revisions(self):
        """
        Delete all the revisions of the segment
        """
        if self.wf_version == 'txlf':
            xml_revisions = self.xml_seg.findall('{urn:oasis:names:tc:xliff:document:1.2}alt-trans')
        else:
            xml_revisions = self.xml_seg.xpath('revisions')
        if xml_revisions:
            for revision in xml_revisions:
                self.xml_seg.remove(revision)
        if self.revisions:
            self.revisions = []
    
    def paste_full_target(self, target: SegmentContent):
        """
        Pastes the full target with the same attributes to the file.
        
        Arguments:
            target (SegmentContent): The segment from where need to be copied the target
        """
        if self.wf_version == 'txlf':
            tgt_elem = self.xml_seg.xpath(
                    'Default:{}'.format('target'), namespaces=txlf_nsmap)[0]
            src_elem = self.xml_seg.xpath(
                    'Default:{}'.format('source'), namespaces=txlf_nsmap)[0]
        else:
            tgt_elem = self.xml_seg.xpath('{}'.format('target'))[0]
            src_elem = self.xml_seg.xpath('{}'.format('source'))[0]
        self.xml_seg.remove(tgt_elem)
        self.xml_seg.insert(self.xml_seg.index(src_elem) + 1, target.xml_text)
        self.target = SegmentContent('target', self.xml_seg, self.wf_version)
        #print('Copy: ', self.target.all_text)
    
    def paste_target(self, target: SegmentContent):
        """
        Paste only the "text", keeping the original values if there was a target
        in the original file.
        
        Arguments:
            target (SegmentContent): The segment from where needs to be copied the target.
        """
        tgt_elem = None
        if self.wf_version == 'txlf':
            try:
                tgt_elem = self.xml_seg.xpath(
                    'Default:{}'.format('target'), namespaces=txlf_nsmap)[0]
            except IndexError:
                pass
            src_elem = self.xml_seg.xpath(
                    'Default:{}'.format('source'), namespaces=txlf_nsmap)[0]
        else:
            try:
                tgt_elem = self.xml_seg.xpath('{}'.format('target'))[0]
            except IndexError:
                pass
            src_elem = self.xml_seg.xpath('{}'.format('source'))[0]
        if tgt_elem is not None:
            for elem in tgt_elem.iterchildren():
                tgt_elem.remove(elem)
        else:
            if self.wf_version == 'txlf':
                tgt_elem = et.Element('{urn:oasis:names:tc:xliff:document:1.2}target', nsmap=txlf_nsmap)
            else:
                tgt_elem = et.Element('target')
            self.xml_seg.insert(self.xml_seg.index(src_elem) +1, tgt_elem)
        
        tgt_elem.text = target.xml_text.text
        if len(list(target.xml_text.iterchildren())) > 0:
            for elem in target.xml_text.iterchildren():
                tgt_elem.append(elem)

    @property
    def is_hidden(self):
        """
        Property of status of the segment. If it is in some way "hidden" or is not.
        It can have this flag enabled in the case of TXLF if the segment is locked or
        the flag translate is setted.
        """
        return self._hidden

    def hide(self, lock: bool=False) -> None:
        """
        Hide a segment. If is 'txml' it hides the segment adding a comment.
        Check what extension is to make one implentation or other.
        """
        if not self._hidden:
            self._hidden = True
            if self.wf_version == 'txml':
                self.__hide_txml()
            elif self.wf_version == 'txlf':
                if lock:
                    self.locked = True
                else:
                    self.hidden = True

    def unhide(self, lock: bool=False):
        """
        Unhide a segment
        """
        if self.wf_version == 'txml':
            self.__unhide_txml()
        else:
            if lock:
                self.locked = False
            else:
                self.hidden = False
        self._hidden = False

    def __unhide_txml(self):
        """
        Method to unhide the TXML segment.
        This method uncomments the TXML segment.
        """
        
        if self.xml_comment is not None:
            new_elem = et.XML(self.xml_comment.text)
            idx = self.xml_comment.getparent().index(self.xml_comment)
            com_parent = self.xml_comment.getparent()
            elements = [element for element in com_parent.iterchildren()]
            elements[idx].getparent().remove(elements[idx])
            self.paragraph.xml_par.insert(idx, new_elem)
            self.xml_seg = self.paragraph.xml_par[idx]
            self.xml_comment = None
            self._hidden = False

    def __hide_txml(self) -> None:
        """
        Hide the segment of the TXML by commenting it
        """
        xml_parent = self.xml_seg.getparent()
        try:
            xml_parent.replace(self.xml_seg, et.Comment(et.tostring(self.xml_seg)))
        except ValueError:
            print(self.id)

    def jsonify(self):
        """
        Serialize the segment to JSON object
        """
        return {
            'id': self.id, 
            'locked': self.locked, 
            'wf_version': self.wf_version, 
            'xml_seg': et.tostring(self.xml_seg, encoding=str),
            'maxlen': self.maxlen,
            'seg_number': self.seg_number,
            'source': self.source.jsonify(),
            'target': self.target.jsonify() if not isinstance(self.target._xml, str) else ''
            }


class Paragraph:
    """
    This class should not be directly instanced. It should be accessed from paragraphs of Wf.
    It reads the paragraphs or blocks of segments of a WordFast file.
    
    Access to the paragraph through the attribute paragraphs of the Wf object.
    
    Example:
        wf = Wf('c:\txlf_to_process\txlf_file.txlf')
        print(len(wf.paragraphs)) # Show the total of paragraphs
    
    Attributes:
        segments (list): Stores all the segments of the paragraph.
        xml_par (Element): Contains the xml of the paragraph element.
        wf_version (str): stores the WordFast version of the file (TXLF or TXML)
        wf (Wf): Reference to the Wf object that is parent of this Paragraph.
    
    """
    def __init__(self, xml_paragraph, wf_version):
        """
        Initialization of the paragraph.
        Arguments:
            xml_paragraph (Element): The XML element of the paragraph
            wf_version (str): WordFast version of the file.
        """
        self.segments = []
        self.__id = ''
        self.__user_attributes = dict()
        self.__attributes = dict()
        self.xml_par = xml_paragraph
        self.wf_version = wf_version
        self.__append_segments()
        self.wf = None
        self.__maxlen = None
        self.__is_segmented = None
        self.__append_hidden_segments()
        if self.wf_version == 'txlf':
            try:
                if self.xml_par.attrib[
                                    '{http://www.gs4tr.org/schema/xliff-ext}segmented']:
                    self.__is_segmented = True
            except KeyError:
                self.__is_segmented = False
    
    @property
    def xml(self) -> et.Element:
        """
        Shows and updates the xml of the Paragraph Element.
        """
        return self.xml_par
    
    @xml.setter
    def xml(self, value) -> None:
        if isinstance(value, et.Element):
            self.xml_par = value
        else:
            raise TypeError('Wrong type, it is not an etree.Element as expected.')
    
    def __append_segments(self):
        """
        Internal method to add the segments to the segments list.
        """
        if self.wf_version == 'txlf':
            xml_segments = self.xml_par.xpath('Default:trans-unit',
                 namespaces=txlf_nsmap)
        else:
            xml_segments = self.xml_par.xpath('./segment')
        self.segments = [Segment(xml_segment, self.wf_version, par_position=idx + 1
                                 ) for idx, xml_segment in enumerate(xml_segments)]
    
    def __append_hidden_segments(self):
        """
        Internal method to find hidden segments in TXML
        """
        if self.wf_version == 'txml':
            xml_hidden = self.xml_par.xpath('comment()')
            for comment in xml_hidden:
                xml_segment = et.XML(comment.text)
                segment = Segment(xml_segment, self.wf_version, hidden=True)
                segment.xml_comment = comment
                self.segments.append(segment)
    
    @property
    def id(self):
        """
        ID of the paragraph. It is a read only property.
        """
        if self.__id:
            return self.__id
        else:
            if self.wf_version == 'txlf':
                self.__id = self.xml_par.attrib['id']
            else:
                self.__id = self.xml_par.attrib['blockId']        
            return self.__id
    
    @property
    def user_attributes(self):
        """
        Return the custom properties of the existing WF file. Custom attributes if it
        is TXLF or satt_something if it is txml.
        """
        key_value_pat_txlf_str = r'(?P<key>\w+?)="(?P<value>.*?)"'
        key_value_txlf_pat = re.compile(key_value_pat_txlf_str)
        if not self.__user_attributes:
            if self.wf_version == 'txlf':
                try:
                    user_attributes = self.xml_par.attrib[
                                    '{http://www.gs4tr.org/schema/xliff-ext}user-attributes']
                except KeyError:
                    user_attributes = dict()
                if user_attributes:
                    key_value_list = key_value_txlf_pat.findall(user_attributes)
                    self.__user_attributes = {key:value for key, value in key_value_list}
            else:
                try:
                    for attribute, value in self.xml_par.attrib.items():
                        if str(attribute).startswith('satt'):
                            self.__user_attributes[attribute] = value
                except KeyError:
                    self.__user_attributes = dict()
                    return self.__user_attributes
        return self.__user_attributes
    
    @property
    def full_paragraph_only_text(self):
        """
        Special property that shows all the text of the paragraph segments together.
        It only reads the source text.
        Read-only property.
        Return:
        full_text (str): Joined text contained in all segments.
        """
        full_text = ''
        
        for segment in self.segments:
            try:
                full_text += segment.source.only_text
            except TypeError:
                pass
                #TODO: Add a logging exception here.
            if self.wf_version == 'txlf':
                ws = segment.xml_seg.xpath('gs4tr:ws', namespaces=txlf_nsmap)
                if ws:
                    try:
                        if ws[0].attrib['{http://www.gs4tr.org/schema/xliff-ext}pos'] == 'after':
                            full_text += segment.source.only_text + ws[0].text
                    except KeyError:
                        pass
        return full_text
    
    @property
    def full_paragraph_src_all_text(self):
        """
        Special property that shows all the text of the paragraph. Including the text 
        between tags.
        Read only property
        Return:
        all_text (str): Joined text contained in all segments.
        """
        all_text = ''
        for segment in self.segments:
            all_text += segment.source.all_text
        return all_text

    @property
    def full_all_source_text(self):
        """
        Special properties to get all the source text included trailing spaces and text between tags.
        Read only property
        Return:
            all_text (str):
        """
        all_text = ''
        for segment in self.segments:
            all_text += segment.source.full_all_text
        return all_text

    @property
    def full_all_target_text(self):
        """
        Special properties to get all the target text included trailing spaces and text between tags.
        Read only property
        Return:
            all_text (str):
        """
        all_text = ''
        for segment in self.segments:
            all_text += segment.target.full_all_text
        return all_text

    @property
    def is_segmented(self):
        """
        Option only available for TXLF files.
        """
        return self.__is_segmented

    def enable_segmented(self, value: bool=True) -> int:
        """
        Enable or disable the segment as segmented.
        
        Arguments:
            value (bool): True to enable 
        """
        if self.wf_version == 'txlf':
            self.__is_segmented = value
            if self.__is_segmented:
                self.xml_par.attrib['{http://www.gs4tr.org/schema/xliff-ext}segmented'] = 'true'
            else:
                try:
                    del self.xml_par.attrib['{http://www.gs4tr.org/schema/xliff-ext}segmented']
                except KeyError:
                    pass
            return 0
        else:
            return -1
    
    @property
    def hidden_segments(self):
        """
        Hidden segments
        """
        for segment in self.hidden_segments:
            return self.__hidden_segments

    def resegment_tag(self, text: str='softReturn'):
        """
        Resegment a paragraph based on the contents of a tag.
        By default it assumes that it contains 'softReturn' value to indicate
        it is a line break. So, it segments based on line break.
        
        Arguments:
            text (str): Value that has to be looked for in the tags.
        """
        print(self.segment_count)
        if self.segment_count == 1:
            seg_temp = self.segments[0]
            
            tags_count_src = seg_temp.source.get_number_tags(text)
            print(tags_count_src)
            id = 1
            if seg_temp.target:
                tags_count_tgt = seg_temp.target.get_number_tags(text)
                print(tags_count_tgt)
                if tags_count_src == tags_count_tgt:
                    src_tags = [tag for tag in self.__divide_segment(seg_temp.source, text)]
                    tgt_tags = [tag for tag in self.__divide_segment(seg_temp.target, text, 'target')]
                    print(src_tags)
                
            if seg_temp.source:
                print(tags_count_src)
                src_tags = [tag for tag in self.__divide_segment(seg_temp.source, text)]

    def __divide_segment(self, seg_cont: SegmentContent, text: str='softReturn', name='source'):
        """
        Divide a segment, not totally implemented yet.
        """
        txt = seg_cont._xml.text
        new_seg = None
        is_new = False
        print(seg_cont._xml.text)
        id = 1
        text_added = False
        for tag in seg_cont._xml.getchildren():
            if is_new:
                new_seg = None
                id = 1
            if text not in tag.text:
                is_new = False
                if new_seg is not None:
                    new_seg += tag
                else:
                    if self.wf_version == 'txml':
                        new_seg = et.Element('segment')
                        new_seg.attrib['segmentId'] = id
                        new_content = et.SubElement(new_seg, name)
                        if not text_added:
                            new_content.text = txt
                            text_added = True
            else:
                is_new = True
                print(txt, new_seg, tag)
                yield txt, new_seg, tag

    def add_user_attribute(self, key, value):
        """
        Add the user attributes to the paragraph.
        The user attributes for the old Wf version (TXML) are added with prefix satt_ and for the new format
        are added as a value for the attribute user-attributes.
        
        Example:
        TXML
        satt_note="Something"
        
        TXLF:
        user-attributes= "&lt;attr note=&quot;Something&quot;&gt;"
        
        Arguments:
            key: Name of the attribute to be added.
        """
        #pdb.set_trace()
        self.__user_attributes[key] = value      
        if self.wf.wf_file_path.is_file():                     
            if self.wf_version == 'txlf':
                try:
                    user_attributes = self.xml_par.attrib[
                                '{http://www.gs4tr.org/schema/xliff-ext}user-attributes']                  
                    attrib_xml = et.fromstring(user_attributes)
                except KeyError:
                    attrib_xml = et.Element('attrib')
                attrib_xml.attrib[key] = value    
                self.xml_par.attrib[
                    '{http://www.gs4tr.org/schema/xliff-ext}user-attributes'] = et.tostring(attrib_xml)
            else:
                self.xml_par.attrib[key] = value
    
    @property
    def attributes(self):
        """
        Get all the attributes of the paragraph.
        It will return the raw attributes and not formatted.
        """
        if not self.__attributes:
            for attribute, value in self.xml_par.attrib.items():
                self.__attributes[attribute] = value
        return self.__attributes
       
    @property
    def maxlen(self):
        """
        Handles the maxlen of a paragraph. Get and set the maxlen of a paragraph.
        """
        if not self.__maxlen and Path(self.wf.wf_file).is_file():
            try:
                if self.wf_version == 'txlf':
                    self.__maxlen = self.xml_par.attrib[
                             '{http://www.gs4tr.org/schema/xliff-ext}maxlen']
                else:
                    self.__maxlen = self.xml_par.attrib['maxlen']
            except KeyError:
                self.__maxlen = None
        return self.__maxlen
        
    @maxlen.setter
    def maxlen(self, value: int):
        self.__maxlen = str(value)
        if Path(self.wf.wf_file).is_file():
            if self.wf_version == 'txlf':
                if self.__maxlen:
                    self.xml_par.attrib[
                                 '{http://www.gs4tr.org/schema/xliff-ext}maxlen'
                                 ] = self.__maxlen
                else:
                    try:
                        self.xml_par.attrib.pop('{http://www.gs4tr.org/schema/xliff-ext}maxlen')
                    except KeyError:
                        pass
            else:
                if self.__maxlen:
                    self.xml_par.attrib['maxlen'] = self.__maxlen
                else:
                    try:
                        self.xml_par.attrib.pop('maxlen')
                    except KeyError:
                        pass
    
    def merge_segments(self):
        """
        Merge the segments in the paragraph into 1 segment.
        And deletes the rest of the segments from the object Paragraph and XML
        """
        if self.segment_count > 1:
            for idx, seg in enumerate(self.segments):
                if idx > 0:
                    self.segments[0].source.paste_segment_content(seg.source, False)
                    if self.segments[0].target and seg.target:
                        print(self.segments[0].target, seg.target)
                        self.segments[0].target.paste_segment_content(seg.target, False)
                    seg.xml.getparent().remove(seg.xml)
            del self.segments[1:]
    
    @property
    def segment_count(self) -> int:
        """
        Return the number of segments in the paragraph.
        """
        return len(self.segments)

    def jsonify(self):
        """
        Return the dictionary serialized object to convert to JSON for API purposes.
        """
        return {
            'id': self.id,
            'is_segmented': self.is_segmented,
            'maxlen': self.maxlen, 
            'user_attributes': self.user_attributes, 
            'attributes': self.attributes,
            'xml_par': et.tostring(self.xml_par, encoding=str),
            'wf_version': self.wf_version, 
            'segments': [segment.jsonify() for segment in self.segments]
            }


class Wf:
    """
    It allows to process a WordFast file.
    
    Example:
        wf = Wf('file')
    
    Attributes:
        wf_file (str): Path string to the WordFast file to be processed.
        wf_file_path (Path): Path objecto the WordFast file path.
        xml_tree (xml_tree): XML tree of the WF file.
        xml_root (Element): Root element of the XML tree.
        paragraphs (list): List that contains the Paragraph objects.
        wf_group (WfGroup): References to the WfGroup class that contains the Wf file.
    
    Properties:
        source_lang (str): Source language of the file. 
        target_lang (str): Target language of the file. It can be empty.
        total_segment_count (int): Read-only property that return the total of segments.
    """
    def __init__(self, wf_file):
        """
        Initialize the class.
        """
        if Path(wf_file).suffix == '.txml' or Path(wf_file).suffix == '.txlf':
            self.wf_file = str(wf_file)
            self.wf_file_path =Path(wf_file)
        else:
            raise ValueError('Wrong format file: It is not a WF valid file.')
        if Path(wf_file).is_file():
            xml_parser = et.XMLParser(encoding='utf-8')
            try:
                self.xml_tree = et.parse(self.wf_file, xml_parser)
            except et.XMLSyntaxError:
                xml_parserhf = et.XMLParser(encoding='utf-8', huge_tree=True)
                try:
                    self.xml_tree = et.parse(self.wf_file, xml_parserhf)
                except et.XMLSyntaxError:
                    self.xml_tree = et.parse(self.wf_file)
            self.xml_root = self.xml_tree.getroot()
            if self.wf_file_path.suffix == '.txml':
                self.wf_version = 'txml'
            elif self.wf_file_path.suffix == '.txlf':
                self.wf_version = 'txlf'
        self.paragraphs = []
        self.__target_language = ''
        self.__source_language = ''
        self.__original_file = ''
        self.__append_paragraphs()
        self._wf_group = None
        self.__total_segment_count = None
        self.__skeleton = None
    
    @property
    def xml(self):
        """
        Access to the xml tree of the file
        """
        return self.xml_tree
    
    @xml.setter
    def xml(self, value: et.ElementTree):
        if isinstance(value, et.ElementTree):
            self.xml_tree = value
    
    def append_group(self, wf_group):
        """
        Add the WordFast object to a WordFast group object. Making persistent the
        object.
        
        Arguments:
        wf_group (WfGroup): Group object where the Wf are added/stored.
        """
        self._wf_group = wf_group
    
    def __append_paragraphs(self):
        """
        Private method to appends the paragraphs to the Wf file.
        """
        if self.wf_version == 'txlf':
            xml_paragraphs = self.xml_tree.xpath("//Default:group[@restype='x-paragraph']",namespaces=txlf_nsmap)
        else:
            xml_paragraphs = self.xml_tree.xpath('translatable')
        #for xml_segment in xml_segments:
            #segment_tmp = Segment(xml_segment)
            #self.segments.append(segment_tmp)
        self.paragraphs = [Paragraph(xml_paragraph, self.wf_version) for xml_paragraph in xml_paragraphs]
        count = 1
        for paragraph in self.paragraphs: 
            paragraph.wf = self       
            for segment in paragraph.segments:
                segment.seg_number = count
                segment.wf = self
                segment.paragraph = paragraph
                count +=1
    
    def __get_attribute(self, element,  attribute, nsmap = None):
        """
        Gets the sepecific attribute
        Arguments:
            element (Element): Element that needs to be processed
            attribute (str): Attribute string name to be processed
        """
        
        if nsmap:
            elements = self.xml_tree.xpath(element, namespaces = nsmap)
            
        else:
            elements = self.xml_tree.xpath(element)
        for element in elements:
            try:
               return element.attrib[attribute]
            except KeyError:
                return -1

    @property
    def skeleton(self):
        """
        Get the skeleton of the original file. Mainly for xml files and other
        text files.
        It is read from the XML file and added to the object.
        """
        if self.__skeleton:
            return self.__skeleton
        else:
            if self.wf_version == 'txlf':
                try:
                    skeleton = self.xml_tree.xpath('//Default:internal-file',
                                     namespaces=txlf_nsmap)[0].text
                except AttributeError:
                    pass
            else:
                try:
                    skeleton = self.xml_tree.xpath('//skeleton')[0].text
                except AttributeError:
                    pass
            self.__skeleton = skeleton
            return self.__skeleton
    
    @skeleton.setter
    def skeleton(self, value: str):
        assert isinstance(value, str)
        if value and isinstance(value, str):
            self.__skeleton = value
            if self.wf_version == 'txlf':
                try:
                    skeleton = self.xml_tree.xpath('//Default:internal-file', 
                                                namespaces=txlf_nsmap)[0]
                except IndexError:
                    self.__skeleton = None
            else:
                try:
                    skeleton = self.xml_tree.xpath('//skeleton')[0]
                except IndexError:
                    self.__skeleton = None
            if skeleton is not None:
                skeleton.text = value
        else:
            raise TypeError('The value is not a string. Please check the type.')
    
    @property
    def target_lang(self):
        """
        Reads the target locale from the TXML file.
        
        Return:
            target_lang (str): Return the target language.
        """
        if self.__target_language:
            return self.__target_language
        else:
            if self.wf_version == 'txlf':
                target_language = self.__get_attribute('//Default:file', 
                    'target-language',
                    nsmap=txlf_nsmap
                    )
            else:
                target_language = self.__get_attribute('//txml', 'targetlocale')
            if target_language != -1:
                self.__target_language = target_language
            else:
                return -1
            return self.__target_language

    @target_lang.setter
    def target_lang(self, lang):
        if '_' in lang:
            lang.replace('_', '-')
        if self.wf_version == 'txlf':
            txlf_element = self.xml_tree.xpath(
                    '//Default:file',
                    namespaces=txlf_nsmap)[0]
            try:
                txlf_element.attrib[
                'target-language'] = lang      
            except:
                return -1
        else:
            txml_elements = self.xml_tree.xpath('//txml')
            try:
                #Loads the element txml that is where are the languages defined
                for element in txml_elements:
                    element.attrib['targetlocale'] = lang
            except TypeError:
                return -1
        #tree = et.ElementTree(self.xml_tree)
        #tree.write(self.wf_file, encoding='utf-8', pretty_print = True)
        self.__target_language = lang
    
    @property
    def source_lang(self):
        """
        Get the source language from the object if it exists or from the associated
        file, if the language was not already set.
        
        Return:
            source_lang (str): Return the soruce language
        """
        if self.__source_language:
            return self.__source_language
        else:
            if self.wf_version == 'txlf':
                source_language = self.__get_attribute(
                    '//Default:file',
                    'source-language',
                    nsmap=txlf_nsmap
                    )
            else:
                source_language = self.__get_attribute('//txml', 'locale')
            if source_language != -1:
                self.__source_language = source_language
            else:
                return -1
            return self.__source_language
    
    @source_lang.setter
    def source_lang(self, lang):
        if '_' in lang:
            lang.replace('_', '-')
        if self.wf_version == 'txlf':
            txlf_element = self.xml_tree.xpath(
                    '//Default:file',
                    namespaces=txlf_nsmap)[0]
            try:
                txlf_element.attrib[
                'source-language'] = lang   
            except:
                return -1
        else:
            txml_elements = self.xml_tree.xpath('//txml')
            try:
                #Loads the element txml that is where are the languages defined
                for element in txml_elements:
                    
                    element.attrib['sourcelocale'] = lang
            except TypeError:
                return -1
        tree = et.ElementTree(self.xml_tree)
        tree.write(self.wf_file, encoding='utf-8', pretty_print = True)
        self.__source_language = lang
    
    @property
    def original_file(self):
        """
        Get the file from what was generated the WF file.
        
        return:
            original_file: Return the path to the source file from which was generated the Wf file.
        """
        if not self.__original_file:
            if self.wf_version == 'txlf':
               
                self.__original_file = self.__get_attribute('//Default:file', 
                                'original', nsmap=txlf_nsmap)

            else:
                self.__original_file = self.__get_attribute('txml', 'file_name')
        return self.__original_file

    @property
    def total_segment_count(self):
        """
        Get the number total of segments that exist in the TXLF file
        
        return:
            original_file: Return the path to the source file from which was generated the Wf file.
        """
        if not self.__total_segment_count:
            if self.wf_version == 'txlf':
               
                self.__total_segment_count = self.__get_attribute('//Default:file', 
                                '{http://www.gs4tr.org/schema/xliff-ext}total-segment-count', nsmap=txlf_nsmap)

            else:
                self.__total_segment_count = self.__get_attribute('txml', 'file_name')
        return self.__total_segment_count

    @property
    def segments(self) -> list:
        """
        Return all the segments of the file.
        """
        return [seg for par in self.paragraphs
                  for seg in par.segments]

    def update_root(self, xml_root):
        """
        Allows to update the root for writing reasons.
        """
        self.xml_root = xml_root
    
    def save(self, file: str='') -> None:
        """
        Save the xml structure of the file. If it is a TXLF is saved as the wf_type.
        This method automatically adds the corresponding extension based on the wf_version.
        Also, allows to save_as if a name is given, otherwise is overwritten.
        It makes deprecated save_xml and save_as methods.
        Arguments:
            file (str): The file name with the file has to be saved. It should go without extension.
        """
        tree = et.ElementTree(self.xml_root)
        if not file:
            filepath = self.wf_file
        else:
            filepath = Path(f'file.{self.wf_version}')
        tree.write(str(filepath), encoding='utf-8', pretty_print=True, 
                      xml_declaration=True)
    
    def save_xml(self):
        """
        Writes the new XML into the file. Save all the changes done in the XML structure.
        Deprecated
        """
        warnings.warn('This method is deprecated, use save instead', DeprecationWarning)
        tree = et.ElementTree(self.xml_root)
        tree.write(str(self.wf_file), encoding='utf-8', pretty_print=True, 
                 xml_declaration=True)

    def save_as(self, filename: str):
        """
        Write the new XML into a new file. Save as new name
        Deprecated
        
        Arguments:
            filename (str): New filename
        """
        warnings.warn('This method is deprecated, use save instead', DeprecationWarning)
        tree = et.ElementTree(self.xml_root)
        tree.write(str(Path(filename)), encoding='utf-8', pretty_print=True, 
                     xml_declaration=True)

    def jsonify(self):
        """
        Serialize Wf object to a JSON object.
        """
        return {
            'source_lang': self.source_lang, 
            'target_lang': self.target_lang, 
            'original_file': str(self.original_file),
            'wf_file': str(self.wf_file_path), 
            'wf_version': self.wf_version,
            'paragraphs': [par.jsonify() for par in self.paragraphs], 
            'segments': [seg.jsonify() for seg in self.segments]
            }

    def find(self, text: str, pos: str='source', case_sensitive: bool=True) -> Segment:
        """
        Find a text in the source, target or both. Return the segment containing this.
        
        Arguments:
            text (str): Text to be searched
            pos (str): Look for in source, target or both. Keywords: source, target, any
            case_sensitive(bool): Flag if the search should be case sensitive or not.
        """
        if not case_sensitive:
            text = text.upper()
        for segment in self.segments:
            if pos == 'source':
                check_text = segment.source.text
            if pos == 'target':
                check_text = segment.target.text
            if pos == 'any':
                check_text = f'{segment.source.text} {segment.target.text}'
            if not case_sensitive:
                check_text = check_text.upper()
            if text in check_text:
                return segment
    
    def findall(self, text: str, pos: str='source', case_sensitive: bool=True) -> list:
        """
        Find a text in the source, target or any and return a list of all segments containing that text.
        
        Arguments:
            text (str): Text to be searched
            pos (str): Look for in source, target or any. Keywords: source, target, any
            case_sensitive (bool): Flag to check if search is sensitive to case or not.
        """
        segments = []
        if not case_sensitive:
            text = text.upper()
        for segment in self.segments:
            if pos == 'source':
                check_text = segment.source.text
            if pos == 'target':
                check_text = segment.target.text
            if pos == 'any':
                check_text = f'{segment.source.text} {segment.target.text}'
            if not case_sensitive:
                check_text = check_text.upper()
            if text in check_text:
                segments.append(segment)
        return segments


if __name__ == '__main__':
    #file = r'j:\A_L\Emirates Group\ES0023794_May_Skywards\8_Rocketmiles 5K bonus MIles - June_25252\99_Reference\LLorenc\TET_Test\txml from PD\Test\Rocketmiles first booking bonus 5K EDM_BU_prepped-MS Excel-de-DE#TR_BSQDS#.xlsx.txml'
    #file = r'q:\zz_Temp\lsuau\Development\Python\pdf_highlight\files\16842 - 16842 - Magnatec Mirror Hanger Stop Start V8-Adobe Indesign V3-de-DE#CHMV#.idml.txml'
    #wf = Wf(file)
    #last_par = wf.paragraphs[-1]
    #last_par.resegment_tag()
    #file = r'q:\zz_Temp\lsuau\Development\Python\wf_det_lang_hidding\files\test\aa_Master\test.txt.txml'
    file = r'p:\production\engineering\Admin_Engineering\_Scripts\Subtitles_SubtitleTemplatePrepQATXLF\TET_TEST\Slide 1.17.srt_fr-FR - Copy.xlsx.txlf'
    file = r'q:\zz_Temp\lsuau\Development\Python\wf_library\files\CDLKA1UXRF0A0-dior_akeneo_xml-de-DE#BDWHFW#.xml.txlf'
    file = r'j:\A_L\Fastly\UK0118098_May\16_FTL-00041-FTL-00040_FSTL-1513-FSTL-1510\04_TM_BE\PMBE\TET_Test\toBlind\FSTL-1510_en-US_de-DE_Translation_de-DE.xliff-TXLF-de-DE#PMBE_BKMYYO#.txlf'
    file = r'p:\production\engineering\Personal\mcabrera\Training\TET_Test\1.Lecture.srt-de-DE_trans.srt-de-DE.xlsx.txlf'
    file = r'q:\zz_Temp\lsuau\Development\Python\wf_library\files\Reviews\test\15646322441320864485521-_-pt-BR-_-en-US-_-tauyou-quintilesSF-_-000-_-emp....txlf'
    file= r'q:\zz_Temp\lsuau\Development\Python\wf_library\files\Reviews\codeofconduct-english-2015-Adobe Indesign INDD-ro-RO#TR_GKJM#.idml-TXLF-ro-RO#IC_BDAJTR#.txlf'
    wf = Wf(file)
    for par in wf.paragraphs:
        par.merge_segments()
    wf.save()
