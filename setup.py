from setuptools import setup

with open('README.md', 'r', encoding='utf-8') as fh:
    long_description = fh.read()

setup(name='wf_library', 

        version='1.0.1', 

        description='Library to process WF files and WF TM.', 
        long_description=long_description, 
        classifiers=[
            'Development Status :: 3 - Alpha', 
            'License :: OSI Approved :: LGPL License 3', 

            'Programming Language :: Python :: 3.8.6', 

            'Topic :: Text Processing :: Translation'
        ], 
        keywords='wf_library tm ap', 
        author='Llorenç Suau', 
        author_email='lsuau@translations.com', 
        license='LGPL 3', 
        packages=['wf_library'], 
        zip_safe=False)
